from decimal import Decimal

from database.models import Menu
from database.repositories.menus import MenuCRUDRepository
from fastapi.responses import JSONResponse
from schemas import PostMenuBodySchema

from .redis_service import RedisService


class MenuService:
    def __init__(
            self,
            repository: MenuCRUDRepository,
            redis: RedisService
    ):
        self.repository = repository
        self.redis = redis

    async def get_one(
            self,
            menu_id: int,
    ) -> Menu:
        result = await self.redis.h_get(f'menu:{menu_id}', 'menu')
        if result is None:
            result = await self.repository.get(menu_id)
            await self.redis.h_set(f'menu:{menu_id}', 'menu', result, 1800)
        return result

    async def get_all(
            self,
    ) -> Menu:
        result = await self.redis.get('menus')
        if result is None:
            result = await self.repository.all()
            await self.redis.set('menus', result, 1800)
        return result

    async def get_full(
            self,
    ) -> list[Menu]:
        result = await self.repository.full()
        for menu in result:
            for submenu in menu.submenus:
                for dish in submenu.dishes:
                    discount = await self.redis.get(f'dish:{dish.id}')
                    if discount is not None:
                        dish.price -= (dish.price * (Decimal(discount / 100)))
        return result

    async def post(
            self,
            body: PostMenuBodySchema
    ) -> Menu:
        result = await self.repository.add(body)
        return result

    async def patch(
            self,
            body: PostMenuBodySchema,
            menu_id: int,
    ) -> Menu:
        result = await self.repository.update(body, menu_id)
        return result

    async def delete(self, menu_id: int) -> JSONResponse:
        result = await self.repository.delete(menu_id)
        return result
