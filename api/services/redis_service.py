import pickle
from typing import Any

import aioredis


class RedisService:
    def __init__(self, client: aioredis.Redis):
        self.client = client

    async def set(self, key: str, data: Any, expire: int) -> None:
        data = pickle.dumps(data)
        await self.client.set(key, data, ex=expire)

    async def get(self, key: str) -> Any:
        data = await self.client.get(key)
        if data is not None:
            return pickle.loads(data)

    async def delete(self, key: str) -> None:
        await self.client.delete(key)

    async def h_set(self, name: str, key: str, data: Any, expire: int) -> None:
        data = pickle.dumps(data)
        await self.client.hset(name, key, data)
        await self.client.expire(name, expire)

    async def h_get(self, name: str, key: str) -> Any:
        data = await self.client.hget(name, key)
        if data is not None:
            return pickle.loads(data)
