from database.models import SubMenu
from database.repositories.submenus import SubMenuCRUDRepository
from fastapi.responses import JSONResponse
from schemas import PostSubMenuBodySchema

from .redis_service import RedisService


class SubMenuService:
    def __init__(
            self,
            repository: SubMenuCRUDRepository,
            redis: RedisService
    ):
        self.repository = repository
        self.redis = redis

    async def get_one(
            self,
            menu_id: int,
            submenu_id: int,
    ) -> SubMenu:
        result = await self.redis.h_get(f'menu:{menu_id}', f'submenu:{submenu_id}')
        if result is None:
            result = await self.repository.get(submenu_id)
            await self.redis.h_set(f'menu:{menu_id}', f'submenu:{submenu_id}', result, 1800)
        return result

    async def get_all(
            self,
            menu_id: int
    ) -> SubMenu:
        result = await self.redis.get('submenus')
        if result is None:
            result = await self.repository.all(menu_id)
            await self.redis.set('submenus', result, 1800)
        return result

    async def post(
            self,
            menu_id: int,
            body: PostSubMenuBodySchema
    ) -> SubMenu:
        result = await self.repository.add(body, menu_id)
        return result

    async def patch(
            self,
            body: PostSubMenuBodySchema,
            submenu_id: int,
    ) -> SubMenu:
        result = await self.repository.update(submenu_id, body)
        return result

    async def delete(self, submenu_id: int) -> JSONResponse:
        result = await self.repository.delete(submenu_id)
        return result
