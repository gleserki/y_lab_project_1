from decimal import Decimal

from database.models import Dish
from database.repositories.dishes import DishCRUDRepository
from fastapi.responses import JSONResponse
from schemas import PostDishBodySchema

from .redis_service import RedisService


class DishService:
    def __init__(
            self,
            repository: DishCRUDRepository,
            redis: RedisService
    ):
        self.repository = repository
        self.redis = redis

    async def get_one(
            self,
            menu_id: int,
            dish_id: int,
    ) -> Dish:
        result = await self.redis.h_get(f'menu:{menu_id}', f'dish:{dish_id}')
        if result is None:
            result = await self.repository.get(dish_id)
            await self.redis.h_set(f'menu:{menu_id}', f'dish:{dish_id}', result, 1800)
        discount = await self.redis.get(f'dish:{dish_id}')
        if discount is not None:
            result.price -= (result.price * (Decimal(discount / 100)))
        return result

    async def get_all(
            self,
            submenu_id: int
    ) -> Dish:
        result = await self.redis.get('dishes')
        if result is None:
            result = await self.repository.all(submenu_id)
            await self.redis.set('dishes', result, 1800)
        for dish in result:
            discount = await self.redis.get(f'dish:{dish.id}')
            if discount is not None:
                dish.price -= (dish.price * (Decimal(discount / 100)))
        return result

    async def post(
            self,
            submenu_id: int,
            body: PostDishBodySchema
    ) -> Dish:
        result = await self.repository.add(submenu_id, body)
        return result

    async def patch(
            self,
            body: PostDishBodySchema,
            dish_id: int,
    ) -> Dish:
        result = await self.repository.update(body, dish_id)
        return result

    async def delete(
            self,
            dish_id: int
    ) -> JSONResponse:
        result = await self.repository.delete(dish_id)
        return result
