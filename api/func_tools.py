from typing import Any

from fastapi import APIRouter
from services.redis_service import RedisService


async def reverse(router: APIRouter, route_name: str, **params: Any) -> str:
    return router.url_path_for(route_name, **params)


async def invalidate_cache(keys: tuple, service: RedisService) -> None:
    for key in keys:
        await service.delete(key)
