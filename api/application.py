from contextlib import asynccontextmanager

from database.connection import Base, engine
from fastapi import FastAPI
from routers import dishes, menus, submenus


@asynccontextmanager
async def lifespan(app: FastAPI):
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
    await engine.dispose()


app = FastAPI(lifespan=lifespan)
app.include_router(menus.router)
app.include_router(submenus.router)
app.include_router(dishes.router)
