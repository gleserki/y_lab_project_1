from dependencies import get_dish_service, get_menu_id, get_submenu_id
from fastapi import APIRouter, BackgroundTasks, Depends, Path
from fastapi.responses import JSONResponse
from func_tools import invalidate_cache
from schemas import DishResponseSchema, PostDishBodySchema
from services.dish_service import DishService

router = APIRouter(
    prefix='/api/v1/menus/{id}/submenus/{submenu_id}',
    tags=['dishes'],
    dependencies=[Depends(get_submenu_id), Depends(get_menu_id)]
)


@router.get(
    '/dishes',
    name='dishes_list', response_model=list[DishResponseSchema]
)
async def get_dishes(
    submenu_id: int, service: DishService = Depends(get_dish_service)
) -> list[DishResponseSchema]:
    return await service.get_all(submenu_id)


@router.get(
    '/dishes/{dish_id}',
    name='get_dish',
    response_model=DishResponseSchema,
    responses={404: {'description': 'Dish not found'}},
)
async def get_dish_by_id(
    dish_id: int, menu_id: int = Depends(get_menu_id), service: DishService = Depends(get_dish_service)
) -> DishResponseSchema:
    return await service.get_one(menu_id, dish_id)


@router.post(
    '/dishes',
    response_model=DishResponseSchema,
    status_code=201,
    name='create_dish',
)
async def post_dish(
    tasks: BackgroundTasks,
    submenu_id: int,
    body: PostDishBodySchema,
    service: DishService = Depends(get_dish_service),
) -> DishResponseSchema:
    tasks.add_task(
        invalidate_cache,
        ('menus', 'submenus', 'dishes'),
        service.redis
    )
    return await service.post(submenu_id, body)


@router.patch(
    '/dishes/{dish_id}', name='update_dish', response_model=DishResponseSchema
)
async def update_dish_by_id(
    tasks: BackgroundTasks,
    body: PostDishBodySchema,
    dish_id: int,
    menu_id: int = Depends(get_menu_id),
    service: DishService = Depends(get_dish_service),
) -> DishResponseSchema:
    tasks.add_task(
        invalidate_cache,
        ('menus', 'submenus', 'dishes', f'menu:{menu_id}'),
        service.redis
    )
    return await service.patch(body, dish_id)


@router.delete('/dishes/{dish_id}', name='delete_dish')
async def delete_dish_by_id(
    tasks: BackgroundTasks,
    dish_id: int,
    menu_id: int = Path(..., alias='id'),
    service: DishService = Depends(get_dish_service),
) -> JSONResponse:
    tasks.add_task(
        invalidate_cache,
        ('menus', 'submenus', 'dishes', f'menu:{menu_id}'),
        service.redis
    )
    return await service.delete(dish_id)
