from dependencies import get_menu_id, get_submenu_service
from fastapi import APIRouter, BackgroundTasks, Depends
from fastapi.responses import JSONResponse
from func_tools import invalidate_cache
from schemas import (
    PostSubMenuBodySchema,
    SubMenuByIdResponseSchema,
    SubMenuResponseSchema,
)
from services.submenu_service import SubMenuService

router = APIRouter(
    prefix='/api/v1/menus/{id}',
    tags=['submenus'],
    dependencies=[Depends(get_menu_id)]
)


@router.post(
    '/submenus',
    response_model=SubMenuResponseSchema,
    status_code=201,
    name='create_submenu',
)
async def post_submenu(
    tasks: BackgroundTasks,
    body: PostSubMenuBodySchema,
    menu_id: int = Depends(get_menu_id),
    service: SubMenuService = Depends(get_submenu_service),
) -> SubMenuResponseSchema:
    tasks.add_task(
        invalidate_cache,
        ('menus', 'submenus'),
        service.redis
    )
    return await service.post(menu_id, body)


@router.get(
    '/submenus',
    name='submenus_list', response_model=list[SubMenuResponseSchema]
)
async def get_submenus(
    menu_id: int = Depends(get_menu_id),
    service: SubMenuService = Depends(get_submenu_service),
) -> list[SubMenuResponseSchema]:
    return await service.get_all(menu_id)


@router.get(
    '/submenus/{submenu_id}',
    name='get_submenu',
    response_model=SubMenuByIdResponseSchema,
    responses={404: {'description': 'Submenu not found'}},
)
async def get_submenu_by_id(
    submenu_id: int, menu_id: int = Depends(get_menu_id), service: SubMenuService = Depends(get_submenu_service)
) -> SubMenuByIdResponseSchema:
    return await service.get_one(menu_id, submenu_id)


@router.patch(
    '/submenus/{submenu_id}',
    name='update_submenu',
    response_model=SubMenuResponseSchema,
)
async def update_submenu_by_id(
    tasks: BackgroundTasks,
    submenu_id: int,
    body: PostSubMenuBodySchema,
    menu_id: int = Depends(get_menu_id),
    service: SubMenuService = Depends(get_submenu_service),
) -> PostSubMenuBodySchema:
    tasks.add_task(
        invalidate_cache,
        ('menus', 'submenus', f'menu:{menu_id}'),
        service.redis
    )
    return await service.patch(body, submenu_id)


@router.delete('/submenus/{submenu_id}', name='delete_submenu')
async def delete_submenu_by_id(
    tasks: BackgroundTasks,
    submenu_id: int,
    service: SubMenuService = Depends(get_submenu_service),
    menu_id: int = Depends(get_menu_id),
) -> JSONResponse:
    tasks.add_task(
        invalidate_cache,
        ('menus', 'submenus', f'menu:{menu_id}'),
        service.redis
    )
    return await service.delete(submenu_id)
