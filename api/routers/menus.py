from dependencies import get_menu_service
from fastapi import APIRouter, BackgroundTasks, Depends, Path
from fastapi.responses import JSONResponse
from func_tools import invalidate_cache
from schemas import (
    FullMenuResponseSchema,
    MenuByIdResponseSchema,
    MenuResponseSchema,
    PostMenuBodySchema,
)
from services.menu_service import MenuService

router = APIRouter(
    prefix='/api/v1/menus',
    tags=['menus'],
)


@router.get('/', name='menus_list', response_model=list[MenuResponseSchema])
async def get_menus(
    service: MenuService = Depends(get_menu_service),
) -> list[MenuResponseSchema]:
    return await service.get_all()


@router.get(
    '/{id}',
    name='get_menu',
    response_model=MenuByIdResponseSchema,
    responses={404: {'description': 'Menu not found'}},
)
async def get_menu_by_id(
    menu_id: int = Path(..., alias='id'),
    service: MenuService = Depends(get_menu_service),
) -> MenuByIdResponseSchema:
    return await service.get_one(menu_id)


@router.get(
    '/full/',
    name='full_menus',
    response_model=list[FullMenuResponseSchema],
)
async def get_menu_full(
    service: MenuService = Depends(get_menu_service),
) -> list[FullMenuResponseSchema]:
    return await service.get_full()


@router.post(
    '/', response_model=MenuResponseSchema, status_code=201, name='create_menu'
)
async def post_menu(
    body: PostMenuBodySchema,
    tasks: BackgroundTasks,
    service: MenuService = Depends(get_menu_service),
) -> MenuResponseSchema:
    tasks.add_task(invalidate_cache, ('menus',), service.redis)
    return await service.post(body)


@router.patch('/{id}', name='update_menu', response_model=MenuResponseSchema)
async def update_menu_by_id(
    body: PostMenuBodySchema,
    tasks: BackgroundTasks,
    menu_id: int = Path(..., alias='id'),
    service: MenuService = Depends(get_menu_service),
) -> MenuResponseSchema:
    tasks.add_task(
        invalidate_cache,
        ('menus', f'menu:{menu_id}'),
        service.redis
    )
    return await service.patch(body, menu_id)


@router.delete('/{id}', name='delete_menu')
async def delete_menu_by_id(
    tasks: BackgroundTasks,
    menu_id: int = Path(..., alias='id'),
    service: MenuService = Depends(get_menu_service)
) -> JSONResponse:
    tasks.add_task(
        invalidate_cache,
        ('menus', f'menu:{menu_id}'),
        service.redis
    )
    return await service.delete(menu_id)
