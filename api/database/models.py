from sqlalchemy import DECIMAL, Column, ForeignKey, Integer, String, Text
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import relationship

from .connection import Base


class Menu(Base):
    __tablename__ = 'menu'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(30), index=True, nullable=False)
    description = Column(Text, index=True)

    submenus = relationship('SubMenu', cascade='all, delete-orphan')
    dishes_proxy = association_proxy('submenus', 'dishes')


class SubMenu(Base):
    __tablename__ = 'submenu'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(30), unique=True, index=True, nullable=False)
    description = Column(Text, index=True)
    menu_id = Column(ForeignKey('menu.id', ondelete='CASCADE'), nullable=False)

    dishes = relationship('Dish', cascade='all, delete-orphan')


class Dish(Base):
    __tablename__ = 'dish'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(30), unique=True, index=True, nullable=False)
    description = Column(Text, index=True)
    price = Column(DECIMAL(precision=10, scale=2), index=True, nullable=False)
    submenu_id = Column(
        ForeignKey('submenu.id', ondelete='CASCADE'),
        nullable=False
    )
