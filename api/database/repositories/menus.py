from database.models import Dish, Menu, SubMenu
from fastapi import HTTPException
from fastapi.responses import JSONResponse
from schemas import PostMenuBodySchema
from sqlalchemy import delete, func, select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload


class MenuCRUDRepository:
    def __init__(self, async_session: AsyncSession):
        self.async_session = async_session

    async def all(self) -> list[Menu]:
        async with self.async_session() as session:
            result = await session.execute(select(Menu))
            menus = result.scalars().all()
        return menus

    async def get(self, menu_id: int) -> Menu:
        async with self.async_session() as session:
            result = await session.execute(
                select(
                    Menu.id,
                    Menu.title,
                    Menu.description,
                    func.count(func.distinct(SubMenu.id)).label('submenus_count'),
                    func.count(func.distinct(Dish.id)).label('dishes_count'),
                )
                .outerjoin(SubMenu, Menu.id == SubMenu.menu_id)
                .outerjoin(Dish, SubMenu.id == Dish.submenu_id)
                .where(Menu.id == menu_id)
                .group_by(Menu)
            )
            menu = result.first()
            if menu is None:
                raise HTTPException(status_code=404, detail='menu not found')
        return menu

    async def add(self, body: PostMenuBodySchema) -> Menu:
        async with self.async_session() as session:
            async with session.begin():
                menu = Menu(**body.dict())
                session.add(menu)
        return menu

    async def update(self, body: PostMenuBodySchema, menu_id: int) -> Menu:
        async with self.async_session() as session:
            async with session.begin():
                result = await session.execute(
                    update(Menu)
                    .where(Menu.id == menu_id)
                    .values(**body.dict())
                    .returning(Menu)
                )
            menu = result.scalar()
        return menu

    async def delete(self, menu_id: int) -> JSONResponse:
        async with self.async_session() as session:
            async with session.begin():
                await session.execute(delete(Menu).where(Menu.id == menu_id))
        return JSONResponse(content={}, status_code=200)

    async def full(self) -> list[Menu]:
        async with self.async_session() as session:
            result = await session.execute(
                select(Menu).options(
                    joinedload(Menu.submenus).joinedload(SubMenu.dishes)
                )
            )
            menus = result.unique().scalars().all()
        return menus
