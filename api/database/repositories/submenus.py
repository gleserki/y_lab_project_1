from database.models import Dish, Menu, SubMenu
from fastapi import HTTPException
from fastapi.responses import JSONResponse
from schemas import PostSubMenuBodySchema
from sqlalchemy import delete, func, select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload


class SubMenuCRUDRepository:
    def __init__(self, async_session: AsyncSession):
        self.async_session = async_session

    async def add(
        self,
        body: PostSubMenuBodySchema,
        menu_id: int,
    ) -> SubMenu:
        async with self.async_session() as session:
            async with session.begin():
                submenu = SubMenu(**body.dict(), menu_id=menu_id)
                session.add(submenu)
        return submenu

    async def all(self, menu_id: int) -> list[SubMenu]:
        async with self.async_session() as session:
            result = await session.execute(
                select(Menu).where(Menu.id == menu_id).
                options(selectinload(Menu.submenus))
            )
            menu = result.scalar()
        return menu.submenus

    async def get(
        self,
        submenu_id: int,
    ) -> SubMenu:
        async with self.async_session() as session:
            result = await session.execute(
                select(
                    SubMenu.id,
                    SubMenu.title,
                    SubMenu.description,
                    func.count(func.distinct(Dish.id)).label('dishes_count'),
                )
                .outerjoin(Dish, SubMenu.id == Dish.submenu_id)
                .where(SubMenu.id == submenu_id)
                .group_by(SubMenu)
            )
            submenu = result.first()
            if submenu is None:
                raise HTTPException(status_code=404, detail='submenu not found')
        return submenu

    async def update(
        self,
        submenu_id: int,
        body: PostSubMenuBodySchema,
    ) -> SubMenu:
        async with self.async_session() as session:
            async with session.begin():
                result = await session.execute(
                    update(SubMenu)
                    .where(SubMenu.id == submenu_id)
                    .values(**body.dict())
                    .returning(SubMenu)
                )
        submenu = result.scalar()
        return submenu

    async def delete(
        self,
        submenu_id: int,
    ) -> JSONResponse:
        async with self.async_session() as session:
            async with session.begin():
                await session.execute(
                    delete(SubMenu).where(SubMenu.id == submenu_id)
                )
        return JSONResponse(content={}, status_code=200)
