from database.models import Dish, SubMenu
from fastapi import HTTPException
from fastapi.responses import JSONResponse
from schemas import PostDishBodySchema
from sqlalchemy import delete, select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload


class DishCRUDRepository:

    def __init__(self, async_session: AsyncSession):
        self.async_session = async_session

    async def all(self, submenu_id: int) -> list[Dish]:
        async with self.async_session() as session:
            result = await session.execute(
                select(SubMenu)
                .where(SubMenu.id == submenu_id)
                .options(selectinload(SubMenu.dishes))
            )
            submenu = result.scalar()
            if submenu is None:
                return []
        return submenu.dishes

    async def get(self, dish_id: int) -> Dish:
        async with self.async_session() as session:
            result = await session.execute(select(Dish).where(Dish.id == dish_id))
            dish = result.scalar()
            if dish is None:
                raise HTTPException(status_code=404, detail='dish not found')
        return dish

    async def add(self, submenu_id: int, body: PostDishBodySchema):
        async with self.async_session() as session:
            async with session.begin():
                dish = Dish(**body.dict(), submenu_id=submenu_id)
                session.add(dish)
        return dish

    async def update(self, body: PostDishBodySchema, dish_id: int):
        async with self.async_session() as session:
            async with session.begin():
                result = await session.execute(
                    update(Dish)
                    .where(Dish.id == dish_id)
                    .values(**body.dict())
                    .returning(Dish)
                )
            dish = result.scalar()
        return dish

    async def delete(self, dish_id: int):
        async with self.async_session() as session:
            async with session.begin():
                await session.execute(delete(Dish).where(Dish.id == dish_id))
        return JSONResponse(content={}, status_code=200)
