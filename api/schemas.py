from pydantic import BaseModel, field_validator


class ResponseSchema(BaseModel):
    id: str
    title: str
    description: str

    @field_validator('id', mode='before')
    def convert_id_to_str(cls, value):
        return str(value)


class BodySchema(BaseModel):
    title: str
    description: str


class MenuResponseSchema(ResponseSchema):
    ...


class SubMenuResponseSchema(ResponseSchema):
    ...


class PostMenuBodySchema(BodySchema):
    ...


class PostSubMenuBodySchema(BodySchema):
    ...


class DishResponseSchema(ResponseSchema):
    price: str

    @field_validator('price', mode='before')
    def convert_price_to_str(cls, value):
        return f'{value:.2f}'


class PostDishBodySchema(BodySchema):
    price: float


class MenuByIdResponseSchema(ResponseSchema):
    submenus_count: int
    dishes_count: int


class SubMenuByIdResponseSchema(ResponseSchema):
    dishes_count: int


class FullSubMenuResponseSchema(ResponseSchema):
    dishes: list[DishResponseSchema]


class FullMenuResponseSchema(ResponseSchema):
    submenus: list[FullSubMenuResponseSchema]
