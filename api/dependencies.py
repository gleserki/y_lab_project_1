import aioredis
from database.connection import async_session
from database.repositories import dishes, menus, submenus
from fastapi import Path
from services import dish_service, menu_service, redis_service, submenu_service


async def get_menu_service() -> menu_service.MenuService:
    redis_client = await aioredis.from_url('redis://redis')
    redis = redis_service.RedisService(redis_client)
    repository = menus.MenuCRUDRepository(async_session)

    return menu_service.MenuService(repository, redis)


async def get_submenu_service() -> submenu_service.SubMenuService:
    redis_client = await aioredis.from_url('redis://redis')
    redis = redis_service.RedisService(redis_client)
    repository = submenus.SubMenuCRUDRepository(async_session)

    return submenu_service.SubMenuService(repository, redis)


async def get_dish_service() -> dish_service.DishService:
    redis_client = await aioredis.from_url('redis://redis')
    redis = redis_service.RedisService(redis_client)
    repository = dishes.DishCRUDRepository(async_session)

    return dish_service.DishService(repository, redis)


async def get_menu_id(menu_id: int = Path(..., alias='id')):
    return menu_id


async def get_submenu_id(submenu_id: int):
    return submenu_id
