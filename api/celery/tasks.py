import asyncio
import pickle

import aioredis
from openpyxl import load_workbook
from sqlalchemy import select, update

from api.database.connection import async_session
from api.database.models import Dish, Menu, SubMenu

from .connection import app


async def create_or_update_menu(menu_id, params: dict):
    async with async_session() as session:
        async with session.begin():
            result = await session.execute(select(Menu).where(Menu.id == menu_id))
            menu = result.scalar()
            if menu is None:
                menu = Menu(**params, id=menu_id)
                session.add(menu)
            else:
                await session.execute(
                    update(Menu)
                    .where(Menu.id == menu_id)
                    .values(params)
                )


async def create_or_update_submenu(menu_id: int, submenu_id: int, params: dict):
    async with async_session() as session:
        async with session.begin():
            result = await session.execute(select(SubMenu).where(SubMenu.id == submenu_id))
            submenu = result.scalar()
            if submenu is None:
                submenu = SubMenu(**params, id=submenu_id, menu_id=menu_id)
                session.add(submenu)
            else:
                await session.execute(
                    update(SubMenu)
                    .where(SubMenu.id == submenu_id)
                    .values(params)
                )


async def create_or_update_dish(submenu_id: int, dish_id: int, params: dict):
    async with async_session() as session:
        async with session.begin():
            result = await session.execute(select(Dish).where(Dish.id == dish_id))
            dish = result.scalar()
            if dish is None:
                dish = Dish(**params, id=dish_id, submenu_id=submenu_id)
                session.add(dish)
            else:
                await session.execute(
                    update(Dish)
                    .where(Dish.id == dish_id)
                    .values(params)
                )


async def synchronize_xlsx_to_db():
    wb = load_workbook('api/admin/Menu.xlsx').active
    menu_id = None
    submenu_id = None
    redis = await aioredis.from_url('redis://redis')
    for row in wb.iter_rows(values_only=True):
        if row[0] is not None:
            menu_id, title, description = row[0:3]
            params = {'title': title, 'description': description}
            await create_or_update_menu(menu_id, params=params)
        elif row[1] is not None:
            submenu_id, title, description = row[1:4]
            params = {'title': title, 'description': description}
            await create_or_update_submenu(menu_id, submenu_id, params=params)
        elif row[2] is not None:
            dish_id, title, description, price, discount = row[2:7]
            await redis.set(f'dish:{dish_id}', pickle.dumps(discount))
            params = {'title': title, 'description': description, 'price': price}
            await create_or_update_dish(submenu_id, dish_id, params=params)
        else:
            ...


@app.task
def main():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(synchronize_xlsx_to_db())
