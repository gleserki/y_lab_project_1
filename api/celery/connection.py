from datetime import timedelta

from celery import Celery

from .config import RABBITMQ_PASS, RABBITMQ_USER

app = Celery(
    'tasks',
    broker=f'amqp://{RABBITMQ_USER}:{RABBITMQ_PASS}@rabbitmq//',
    backend='rpc://'
)

app.conf.beat_schedule = {
    'synchronize_xlsx_to_db': {
        'task': 'api.celery.tasks.main',
        'schedule': timedelta(seconds=15),
    },
}
