# Y_LAB_project_1 | FastAPI

API для сайта-ресторана, который реализует базовые операции CRUD

## Contents

- [App Structure](#app-structure)
- [Technologies](#technologies)
- [How to launch application](#how-to-launch-application)
- [How to view documentation](#how-to-view-documentation)
- [Analog Django Reverse](#analog-django-reverse)
- [Completed Tasks](#completed-tasks)

## App Structure

- [api](#api)
  - [Database](#database)
  - [Routers](#routers)
  - [Other](#other)

## Technologies

- **FastAPI** - Для быстрых запросов с использованием асинхронности
- **PostgreSQL** - Для удобного хранения данных. Имеет множество типов данных, что позволяет использовать такой тип данных как ARRAY и др
- **SQLAlchemy ORM** - Для создания моделей/отношений на основе (Python Class) и запросов к бд

## api

### Database

#### files

- **repositories** - Бизнес логика по работе с БД
- **config** - Переменные окружения, необходимые для работы с БД
- **connection** - найстройки самого подключения, определение session, engine
- **models** - ORM модели

### Routers

- **dishes** - CRUD(создание, чтение и т.д) для блюд
- **menus** - CRUD(создание, чтение и т.д) для меню
- **submenus** - CRUD(создание, чтение и т.д) для подменю

### Other

- **admin** - Файлы с данными БД
- **celery** - Таски для celery
- **services** - Сервисы, такие как RedisService и т.д
- **func_tools** - Функции помошники
- **dependencies** - Зависимости для fastapi
- **application** - Основной файл для запуска приложения
- **schemas** - схемы pydantic для валидации/сериализации данных

## Analog Django Reverse

- `api/func_tools` - Здесь находится функция reverse

## How to launch application

Создать .env файл на основе шаблона с настройками подключения, например

```
  POSTGRES_USER: example
  POSTGRES_PASSWORD: example
  POSTGRES_DB: example
  RABBITMQ_DEFAULT_USER: root
  RABBITMQ_DEFAULT_PASS: password
  DATABASE_URL: "postgresql+asyncpg://<POSTGRES_USER>:<POSTGRES_PASSWORD>@postgres/<POSTGRES_DB>"
```

- `docker compose -f docker-compose-prod.yml up -d` - Запуск контейнеров Postgres, Api - приложения

## How to run test enviroment

- `docker compose -f docker-compose-test.yml up -d` - Запуск контейнеров Postgres, Тестового окружения

## How to view documentation

- `/docs` - Ссылка для просмотра документации

## Completed Tasks

* Количество подменю и блюд через один ORM запрос - `api/database/repositories/menus.py`
* Тестовый сценарий «Проверка кол-ва блюд и подменю в меню» - `tests/test_dishes_&_submenus_count.py`
* Описать ручки API в соответствий c OpenAPI - `api/routers/`
* Реализовать в тестах аналог Django reverse() для FastAPI - `api/func_tools/`
* Обновление меню из google sheets раз в 15 сек - `api/celery/tasks.py`
* Блюда по акции. Размер скидки (%) указывается в столбце G файла Menu.xlsx - `api/services/dish_service.py`
