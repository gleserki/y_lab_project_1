import pytest

from api.func_tools import reverse
from api.routers import menus, submenus


@pytest.mark.asyncio
async def test_create_menu(client):
    data = {
        'title': 'My menu 1',
        'description': 'My menu description 1'
    }
    url = await reverse(menus.router, 'create_menu')
    response = await client.post(url, json=data)
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_create_submenu(client):
    data = {
        'title': 'My submenu 1',
        'description': 'My submenu description 1'
    }
    url = await reverse(
        submenus.router, 'create_submenu',
        id=1
    )
    response = await client.post(url, json=data)
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_check_submenu_created(client):
    url = await reverse(
        submenus.router, 'get_submenu',
        id=1, submenu_id=1
    )
    response = await client.get(url)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data.get('id', False)
    assert response_data['title'] == 'My submenu 1'
    assert response_data['description'] == 'My submenu description 1'


@pytest.mark.asyncio
async def test_patch_submenu(client):
    data = {
        'title': 'My updated submenu 1',
        'description': 'My updated submenu description 1'
    }
    url = await reverse(
        submenus.router, 'update_submenu',
        id=1, submenu_id=1
    )
    response = await client.patch(url, json=data)
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_check_submenu_pathed(client):
    url = await reverse(
        submenus.router, 'get_submenu',
        id=1, submenu_id=1
    )
    response = await client.get(url)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data.get('id', False)
    assert response_data['title'] == 'My updated submenu 1'
    assert response_data['description'] == 'My updated submenu description 1'


@pytest.mark.asyncio
async def test_delete_submenu(client):
    url = await reverse(
        submenus.router, 'delete_submenu',
        id=1, submenu_id=1
    )
    response = await client.delete(url)
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_check_submenu_deleted(client):
    url = await reverse(
        submenus.router, 'get_submenu',
        id=1, submenu_id=1
    )
    response = await client.get(url)
    assert response.status_code == 404
    assert response.json() == {
        'detail': 'submenu not found'
    }
