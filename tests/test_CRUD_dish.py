import pytest

from api.func_tools import reverse
from api.routers import dishes, menus, submenus


@pytest.mark.asyncio
async def test_create_menu(client):
    data = {
        'title': 'My menu 1',
        'description': 'My menu description 1'
    }
    url = await reverse(menus.router, 'create_menu')
    response = await client.post(url, json=data)
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_create_submenu(client):
    data = {
        'title': 'My submenu 1',
        'description': 'My submenu description 1'
    }
    url = await reverse(
        submenus.router, 'create_submenu',
        id=1
    )
    response = await client.post(url, json=data)
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_create_dish(client):
    data = {
        'title': 'My dish 1',
        'description': 'My dish description 1',
        'price': '12.50'
    }
    url = await reverse(
        dishes.router, 'create_dish',
        id=1, submenu_id=1
    )
    response = await client.post(url, json=data)
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_check_dish_created(client):
    url = await reverse(
        dishes.router, 'get_dish',
        id=1, submenu_id=1, dish_id=1
    )
    response = await client.get(url)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data.get('id', False)
    assert response_data['title'] == 'My dish 1'
    assert response_data['description'] == 'My dish description 1'
    assert response_data['price'] == '12.50'


@pytest.mark.asyncio
async def test_patch_dish(client):
    data = {
        'title': 'My updated dish 1',
        'description': 'My updated dish description 1',
        'price': '14.50',
    }
    url = await reverse(
        dishes.router, 'update_dish',
        id=1, submenu_id=1, dish_id=1
    )
    response = await client.patch(url, json=data)
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_check_dish_pathed(client):
    url = await reverse(
        dishes.router, 'get_dish',
        id=1, submenu_id=1, dish_id=1
    )
    response = await client.get(url)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data.get('id', False)
    assert response_data['title'] == 'My updated dish 1'
    assert response_data['description'] == 'My updated dish description 1'
    assert response_data['price'] == '14.50'


@pytest.mark.asyncio
async def test_delete_dish(client):
    url = await reverse(
        dishes.router, 'delete_dish',
        id=1, submenu_id=1, dish_id=1
    )
    response = await client.delete(url)
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_check_dish_deleted(client):
    url = await reverse(
        dishes.router, 'get_dish',
        id=1, submenu_id=1, dish_id=1
    )
    response = await client.get(url)
    assert response.status_code == 404
    assert response.json() == {
        'detail': 'dish not found'
    }
