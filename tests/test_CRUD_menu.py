import pytest

from api.func_tools import reverse
from api.routers import menus


@pytest.mark.asyncio
async def test_check_menu_list(client):
    url = await reverse(menus.router, 'menus_list')
    response = await client.get(url)
    assert response.status_code == 200
    assert response.json() == []


@pytest.mark.asyncio
async def test_create_menu(client):
    data = {
        'title': 'My menu 1',
        'description': 'My menu description 1'
    }
    url = await reverse(menus.router, 'create_menu')
    response = await client.post(url, json=data)
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_check_menu_created(client):
    url = await reverse(
        menus.router, 'get_menu',
        id=1
    )
    response = await client.get(url)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data.get('id', False)
    assert response_data['title'] == 'My menu 1'
    assert response_data['description'] == 'My menu description 1'


@pytest.mark.asyncio
async def test_patch_menu(client):
    data = {
        'title': 'My updated menu 1',
        'description': 'My updated menu description 1'
    }
    url = await reverse(
        menus.router, 'update_menu',
        id=1
    )
    response = await client.patch(url, json=data)
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_check_menu_pathed(client):
    url = await reverse(
        menus.router, 'get_menu',
        id=1
    )
    response = await client.get(url)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data.get('id', False)
    assert response_data['title'] == 'My updated menu 1'
    assert response_data['description'] == 'My updated menu description 1'


@pytest.mark.asyncio
async def test_delete_menu(client):
    url = await reverse(
        menus.router, 'delete_menu',
        id=1
    )
    response = await client.delete(url)
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_check_menu_deleted(client):
    url = await reverse(
        menus.router, 'get_menu',
        id=1
    )
    response = await client.get(url)
    assert response.status_code == 404
    assert response.json() == {
        'detail': 'menu not found'
    }
