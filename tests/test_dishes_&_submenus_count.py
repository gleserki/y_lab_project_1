import pytest

from api.func_tools import reverse
from api.routers import dishes, menus, submenus


@pytest.mark.asyncio
async def test_create_menu(client):
    data = {
        'title': 'My menu 1',
        'description': 'My menu description 1'
    }
    url = await reverse(menus.router, 'create_menu')
    response = await client.post(url, json=data)
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_create_second_menu(client):
    data = {
        'title': 'My menu 2',
        'description': 'My menu description 2'
    }
    url = await reverse(menus.router, 'create_menu')
    response = await client.post(url, json=data)
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_create_submenu(client):
    data = {
        'title': 'My submenu 1',
        'description': 'My submenu description 1'
    }
    url = await reverse(
        submenus.router, 'create_submenu',
        id=1
    )
    response = await client.post(url, json=data)
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_create_first_dish(client):
    data = {
        'title': 'My dish 1',
        'description': 'My dish description 1',
        'price': '13.50'
    }
    url = await reverse(
        dishes.router, 'create_dish',
        id=1, submenu_id=1
    )
    response = await client.post(url, json=data)
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_create_second_dish(client):
    data = {
        'title': 'My dish 2',
        'description': 'My dish description 2',
        'price': '14.50'
    }
    url = await reverse(
        dishes.router, 'create_dish',
        id=1, submenu_id=1
    )
    response = await client.post(url, json=data)
    assert response.status_code == 201


@pytest.mark.asyncio
async def test_check_menu(client):
    url = await reverse(
        menus.router, 'get_menu',
        id=1
    )
    response = await client.get(url)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data.get('id', False)
    assert response_data['title'] == 'My menu 1'
    assert response_data['description'] == 'My menu description 1'
    assert response_data['dishes_count'] == 2
    assert response_data['submenus_count'] == 1


@pytest.mark.asyncio
async def test_check_full_menus(client):
    url = await reverse(
        menus.router, 'full_menus',
    )
    response = await client.get(url)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data[0].get('id', False)
    assert response_data[0]['title'] == 'My menu 1'
    assert response_data[1]['title'] == 'My menu 2'
    assert response_data[0].get('submenus', False)
    assert response_data[0]['submenus'][0].get('dishes', False)


@pytest.mark.asyncio
async def test_check_submenu(client):
    url = await reverse(
        submenus.router, 'get_submenu',
        id=1, submenu_id=1
    )
    response = await client.get(url)
    assert response.status_code == 200
    response_data = response.json()
    assert response_data.get('id', False)
    assert response_data['title'] == 'My submenu 1'
    assert response_data['description'] == 'My submenu description 1'
    assert response_data['dishes_count'] == 2


@pytest.mark.asyncio
async def test_delete_submenu(client):
    url = await reverse(
        submenus.router, 'delete_submenu',
        id=1, submenu_id=1
    )
    response = await client.delete(url)
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_check_dishes(client):
    url = await reverse(
        dishes.router, 'dishes_list',
        id=1, submenu_id=1
    )
    response = await client.get(url)
    assert response.status_code == 200
    assert response.json() == []


@pytest.mark.asyncio
async def test_check_submenu_deleted(client):
    url = await reverse(
        submenus.router, 'get_submenu',
        id=1, submenu_id=1
    )
    response = await client.get(url)
    assert response.status_code == 404
    assert response.json() == {
        'detail': 'submenu not found'
    }
